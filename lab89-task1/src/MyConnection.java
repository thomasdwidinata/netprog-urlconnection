import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;

public class MyConnection {
	URL targetUrl = null; // Declare variable URL for creating new object of an URL data type
	URLConnection urlConnection = null; // Declare URLConnection for storing the URL Connection
	BufferedReader urlReader = null; // Declare Buffered Reader for creating Reader Instance from URLConnection
	StringBuilder str = new StringBuilder();
	Boolean ready = false;
	
	public MyConnection(String url){
		try {
			targetUrl = new URL(url); // Create URL Object
			urlConnection = targetUrl.openConnection(); // Open the connection using URL object
			urlReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream())); // If connection established, get the Input Stream and assign it to the object
			this.ready = true;
		} catch (MalformedURLException e) { // Catch exception if wrong URL is received
			System.err.println("The URL received is not valid!");
		} catch (IOException e) { // Catch exception if something else happends regarding to IO Connection
			System.err.println("Error: " + e.getMessage());
		}
	}
	
	public Boolean isReady() {
		return this.ready;
	}
	
	public String read(){
		String cache; // Declaring cache to save the temporary data
		str = new StringBuilder(); // Always clean up the 'str' variable before using it!
		try {
			while ((cache = urlReader.readLine()) != null) // Read Line from the BufferedReader
				str.append(cache); // Append to the StringBuilder
		} catch (IOException e) {
			e.printStackTrace();
		}
		return str.toString(); // Return the content of the URL
	}
	
	public String getIp(){
		String ip = new String(); // Declaring temporary String
		try {
			InetAddress address = InetAddress.getByName(targetUrl.getHost()); // Declaring InetAddress to get the IP Address of the host
			ip = address.getHostAddress(); // After connection established, get the Host IP Address
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		return ip;
	}
	
	public String getContentType() {
		String content = new String();
		try {
			content = targetUrl.getContent().toString(); // To get the content type, either HTTP connection or FTP or something else
		} catch (IOException e) {
			e.printStackTrace();
		}
		return content;
	}
	
	public void close() {
		try {
			urlReader.close(); // Close the Buffered Reader
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public int getDataSize() {
		return str.length(); // Get the size of the data that has been read before
	}

}
