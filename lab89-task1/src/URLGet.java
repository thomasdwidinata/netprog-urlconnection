import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.util.Scanner;

/**
 * 
 * Lab 8 - 9 Practise
 * 
 * Task 1 - URL Connection
 * 
 * The target is http://m1-c45n1.csit.rmit.edu.au/~Course/index.php. a) Make a
 * URL connection to the target. Get the target's IP address, content type and
 * length of content. Print all these to the screen. b) Get the data content
 * from the target and print it out to the screen.
 * 
 * @author thomasdwidinata
 *
 */

public class URLGet {

	public static void main(String args[]) {
		Scanner scan = new Scanner(System.in); // Declaring Scanner to ask user which URL that they want
		String url = new String(""); // Declaring variable to store the URL
		MyConnection conn = null; // Creating new variable of my own Class
		
		// Welcome screen, it will ask the user to input the URL, if invalid or blank, try again
		System.out.printf("Hello! Which URL you want me to get?\nEnter the URL: ");
		while (url.equals(""))
			url = scan.nextLine();

		conn = new MyConnection(url); // Creating new instance of MyConnection and automatically connects to the specified URL
		if(conn.isReady())
		{
			System.out.printf("The content of the URL is : \n %s\n", conn.read()); // Print the content of the URL
			URLGet.printInfo(conn.getIp(), conn.getContentType(), conn.getDataSize()); // Get all required information fromt the object and pass it into the printer
			
			scan.close(); // Close the scanner, indicating program is finishing and will not receive input
			System.out.printf("Finish opening connection to %s\n", url);
		}
	}

	public static void printInfo(String ip, String content, int length) {
		System.out.println("\n\n********** CONNECTION INFORMATION **********");
		System.out.printf("Host IP\t\t: %s\n", ip); // Prints the IP Address of the Host
		System.out.printf("Content Type\t: %s\n", content); // The Content Type
		System.out.printf("Size\t\t: %d Bytes\n", length); // The size of the content read from the URL, in Bytes
	}
}
