# Network Progamming (COSC 1179)

## Lab Week 8-9 Exercise

### Description
The purpose of this exercise is to make a Java program that opens URL Connection using HTTP protocol and JAR Protocol. The program will ask the user which URL that the program should try to connect and get the information about it.

#### First Task (URL Connection using HTTP Protocol)
The program will ask the user to input the URL that the program should connect to. And then the program will connect and retrieve the data received from the server and print it. Besides printing the data that has been captured by the program, it will also display the connection information.

#### Second Task (URL Connection using JAR HTTP Protocol)
Almost the same as First Task, but instead it uses JarUrlConnection and download the JAR file instead of getting the raw data from the server and treat it as plain text. The program will also display the content of the JAR file.
