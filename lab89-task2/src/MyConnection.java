import java.io.IOException;
import java.io.InputStream;
import java.net.JarURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MyConnection {
	URL targetUrl = null; // Declare variable URL for creating new object of an URL data type
	JarURLConnection jarURL = null; // Declare Jar Connection for starting the URL Connection. This class extends the URLConnection so it can be replaced with URLConnection instead of JarURLConnection. I use this because it is rich of JAR's feature
	JarFile myJar = null; // Declare to save the JAR Object
	Boolean ready = false;
	
	public MyConnection(String url) {
		Pattern regex = null;
		Matcher match = null;
		try {
			regex = Pattern.compile("^jar\\:.*\\!\\/$");
			match = regex.matcher(url);
			if(!match.find()) {
				System.out.printf("Concating string with 'jar:' and '!/'...");
				url = "jar:" + url + "!/";
				System.out.println("New URL : " + url);
			}
			targetUrl = new URL(url); // Create URL Object
			jarURL = (JarURLConnection) targetUrl.openConnection(); // Connects to the specified URL and check the JAR File
			myJar = jarURL.getJarFile(); // Get the JAR file if available
			this.ready = true;
		} catch (MalformedURLException e) { // Catch exception if wrong URL is received
			System.err.println("The JAR URL received is not valid!");
		} catch (IOException e) { // Catch exception if something else happends regarding to IO Connection
			System.err.println("Error: " + e.getMessage());
		}
	}
	
	public Boolean isReady() {
		return this.ready;
	}
	
	public String getJarName() {
		return myJar.getName(); // Get the JAR Name
	}
	
	public String getContentType() {
		return jarURL.getContentType(); // Get the connection content type, this should be java's JAR
	}
	
	public int getSize() {
		return jarURL.getContentLength(); // Get the size of the JAR file
	}
	
	public void getEntries() throws IOException {
		Enumeration<JarEntry> jarEntries = myJar.entries(); // Get all JAR file Entries
		JarEntry current = null; // Variable to save the Current iteration of JarEntry
		InputStream is = null;;
		int iteration = 1; // Saves the current iteration number
		while(jarEntries.hasMoreElements()) { // Loop to get all the data inside jarEntries
			current = jarEntries.nextElement(); // Get the next element
			is = myJar.getInputStream(current);
			System.out.printf("%d)\n", iteration++); // Increment iteration number after printing
			System.out.printf("\tName\t: %s\n", current.getName()); // Get the content Name
			System.out.printf("\tSize\t: %d bytes\n", (int) current.getSize()); // Get the content size
			System.out.printf("\tContent\t: ");
			while(is.available() != 0) // Read the content of each file inside the JAR
				System.out.print((char) is.read()); // Read one by one char, because is.read() will
													// return integer, we should convert to char!
			System.out.println("\n");
		}
	}
}
