import java.io.IOException;
import java.net.JarURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Scanner;
import java.util.jar.JarFile;
import java.util.jar.Manifest;


/**
 * 
 * Lab 8 - 9 Practise
 * 
 * Task 2 - Getting JAR from specified URL
 * 
 * The target is jar:http://m1-c45n1.csit.rmit.edu.au/~Course/HelloWorld.jar!/
 * a) Make a jarURL connection to the target. Again, get the target's content
 * type and length of content, and print them to the screen.
 * b) Get a list of jar entries (names and sizes) from the target and print it
 * out to the screen.
 * 
 * @author thomasdwidinata
 *
 */

public class JARGet {
	
	public static void main(String args[]) {
		Scanner scan = new Scanner(System.in); // Declaring Scanner to ask user which URL that they want
		String url = new String(""); // Declaring variable to store the URL
		MyConnection conn = null; // Creating new variable of my own Class
		
		// Welcome screen, it will ask the user to input the URL, if invalid or blank, try again
		System.out.printf("Hello! Which JAR file you want me to get?\nEnter the URL: ");
		while (url.equals(""))
			url = scan.nextLine();

		conn = new MyConnection(url); // Creating new instance of MyConnection and automatically connects to the specified URL to get the JAR file
		if(conn.isReady())
		{
			JARGet.printInfo(conn.getJarName(), conn.getContentType(), conn.getSize()); // Get all required information fromt the object and pass it into the printer
			try{
				System.out.println("Inside the JAR File:");
				conn.getEntries();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			scan.close();
			System.out.printf("Finish opening connection to %s\n", url);
		}
	}
	
	public static void printInfo(String jarName, String contentType, int length) {
		System.out.println("\n\n********** CONNECTION INFORMATION **********");
		System.out.printf("JAR File Name\t: %s\n", jarName); // Print the name of the Jar
		System.out.printf("Type\t\t: %s\n", contentType); // Print the content type of the connection
		System.out.printf("Size of File\t: %d Bytes\n", length); // Print the file size (JAR)
	}
}
